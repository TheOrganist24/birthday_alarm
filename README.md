![TheOrganist24 Code](https://hosted.courtman.me.uk/img/logos/theorganist24_banner_code.png "TheOrganist24 Code")

# BIRTHDAY_ALARM
> Birthday Alarm

## Getting Started
```bash
poetry run birthday_alarm
```


## Development
### Setting up Environment
```bash
git clone git@gitlab.com:TheOrganist24/birthday_alarm.git
cd birthday_alarm
make dev
export LOG_LEVEL=INFO  # Optional; supports DEBUG, INFO, WARNING, ERROR, CRITICAL
poetry run birthday_alarm --version
```


### Versioning
Semantic versioning is used:
```
poetry version <MAJOR,MINOR,PATCH>
poetry version --short > VERSION
```

Also don't forget to update `birthday_alarm/__init__.py` and `tests/test_birthday_alarm.py`.


### Lint and Test
Code should be compliant with PEPs 8, 256, 484, and 526.
```bash
make check  # calls `make lint; make test`
```
